import Element from "./Elements.js";

import LogIn from './Form.js';
import CONSTANTS from "./constants.js";

const { ROOT } = CONSTANTS;

export default class Modal{
    constructor(id, classes, text){
        this.id = id;

        this.classes = classes;

        this.text = text;
    }

    render(){
        
        
        let text = new Element('p', ['bla'], '', 'Heloooo').createElement()



                                                            // let passwd_label = new Element('label', ['form-label'], '', 'Password').createElement()
                                                            // passwd_label.for='inputPassword2'

                                                            // let passwd_input = new Element('input', ['form-control'], "inputPassword2",'').createElement()
                                                            // passwd_input.placehoder = "Password"
                                                            // passwd_input.type = 'password'

                                                            // let fragment2 = document.createDocumentFragment()
                                                            // fragment2.append(passwd_label, passwd_input)
                                                            // let passw = new Element('div', ['mb-2'], "", fragment2).createElement()


                                                            // let login_label = new Element('label', ['form-label'], '', 'Email').createElement()
                                                            // login_label.for='staticEmail2'

                                                            // let login_input = new Element('input', ['form-control'], "exampleFormControlInput1",'').createElement()
                                                            // login_input.placehoder = "name@example.com"
                                                            // login_input.type = 'email'
                                                            // let fragment1 = document.createDocumentFragment()
                                                            // fragment1.append(login_label, login_input)
                                                            // let login = new Element('div', ['mb-2'], "",fragment1).createElement()



        let loginForm = new LogIn(['login-form'], '1').render()
        console.log(loginForm)
        let closeBtn = new Element('span', ['close'], '', '&times;').createElement()

        let fragment = document.createDocumentFragment()
        fragment.append(closeBtn, text,  loginForm)

        let modalContent = new Element('div', ['modal-content'], '', fragment).createElement()

        let modal = new Element('div', this.id, this.classes, modalContent ).createElement()


        this._modal = modal;
        
        closeBtn.addEventListener('click', ()=>{
            this._modal.classList.remove('active')
        })
    }

    openModal(){
        ROOT.append(this._modal);
        this._modal.classList.add('active')
    }
}