export default class Element{
    constructor(tag, classes, id, content){
        this.tag = tag
        this.classes = classes;
        this.id = id;
        this.content = content;
    }

    createElement(){
        let element = document.createElement(this.tag);


        element.className = this.classes.join(' ');
        

        element.id = this.id;

        if (typeof this.content !== "object") {
            element.innerHTML = this.content;
        } else {
            element.append(this.content);
        }

        return element
    }
}