import Element from "./Elements.js";
 class Form {
    constructor( classes, id, action=''){
        this.id = id;
        this.classes = classes;
        this.action = action;
        this._fields = this.createFields();
    }

    render(){
        let form = new Element('form', this.classes, this.id, this._fields).createElement()
        form.action = this.action

        form.addEventListener("submit", this.handleSumbit.bind(this));

        return form;
    }


    handleSumbit(event){
        event.preventDefault();
        let formInputs = [...event.target.elements];
        console.log(formInputs)
    }
}



export default class LogIn extends Form{
    constructor(classes, id){
        super(classes, id)
    }

    createFields(){
        let EmailLabel = new Element('label', ['1'], '', 'Email address').createElement()
        EmailLabel.for = "exampleInputEmail1";

        let EmailInput = new Element('input', ['form-control'], 'exampleInputEmail1', '').createElement()
        EmailInput.type = 'email';
        EmailInput.placeholder = 'Enter email'

        let EmailSmall = new Element('small', ["form-text", "text-muted"], 'emailHelp', "We'll never share your email with anyone else.").createElement()

        let EmailFragment = document.createDocumentFragment()
        EmailFragment.append(EmailLabel, EmailInput, EmailSmall)

        let EmailGroup = new Element('div', ['form-group', ''], '', EmailFragment).createElement()

        let PasswdLabel = new Element('label', ['1'], '', 'Password').createElement()
        PasswdLabel.for = "exampleInputPassword1";

        let PasswdInput = new Element('input', ['form-control', 'mb-2'], 'exampleInputPassword1', '').createElement()
        PasswdInput.type = 'password';
        PasswdInput.placeholder = 'Password'

        let PasswdFragment = document.createDocumentFragment()

        PasswdFragment.append(PasswdLabel, PasswdInput)

        let PasswdGroup = new Element('div', ['form-group', 'mb-3', 'mt-3'], '', PasswdFragment).createElement()

        let btn = new Element('button', ['btn', 'btn-primary'], 'login_submit', 'Submit').createElement()
        btn.type = 'submit'

        let LogInFormFragment=document.createDocumentFragment()
        LogInFormFragment.append(EmailGroup, PasswdGroup, btn)
        
        return LogInFormFragment

    }
}