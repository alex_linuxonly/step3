import CONSTANTS from './constants.js'
import Modal from './Modal.js'
const { ROOT, LOGIN_BTN } = CONSTANTS

const modal = new Modal(['modal'], 'modal', "Hello")

modal.render(ROOT)


LOGIN_BTN.addEventListener('click', ()=>{
    modal.openModal()
})